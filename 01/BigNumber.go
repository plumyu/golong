package main

import (
	"fmt"
	"math/big"
)

// go 大数可以引入 big 包
func main() {
	const lightSpeed = 299792
	const secondsPerDay = 86400
	var distance int64 = 41.3e12
	fmt.Println("alpha centauri  is", distance, "km away.")
	days := distance / lightSpeed / secondsPerDay
	fmt.Println("That is", days, "days of travel at light  speed.")

	// distance = 24e18
	// float64 可以 -> 2.4e19

	ls := big.NewInt(299792)
	sp := big.NewInt(86400)
	fmt.Println(ls, sp)

	di := new(big.Int)
	di.SetString("24000000000000000000000000", 10)
	fmt.Println(di)
}
