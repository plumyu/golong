package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// 生成随机数种子
	rand.Seed(time.Now().UnixNano())

	// 生成 1 到 100 之间的随机数
	target := rand.Intn(100) + 1
	fmt.Println("欢迎参加猜数游戏！")
	fmt.Println("猜一个 1 到 100 之间的数:")

	// 循环读取用户猜测的数字
	for {
		var guess int
		_, err := fmt.Scanf("%d", &guess)
		if err != nil {
			fmt.Println("输入无效，请输入一个有效的整数。")
			continue
		}

		// 检查猜测的数字与目标数字的关系
		if guess < target {
			fmt.Println("太小了！再试一次:")
		} else if guess > target {
			fmt.Println("太大了！再试一次:")
		} else {
			fmt.Println("恭喜你，猜对了！")
			break
		}
	}
}
