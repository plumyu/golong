package main

import (
	"fmt"
)

func main() {
	var count = 10

	for count > 0 {
		fmt.Println(count)
		time.sleep(time.second)
		count--
	}
	fmt.Println("liftoff!")
}
