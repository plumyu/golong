package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println("You find yourself in a dimly lit cavern.")

	var command = "walk outside"
	// strings 包含函数
	var exit = strings.Contains(command, "outside")
	//
	fmt.Println("You leave the cave:", exit)
	fmt.Println("----------------------------------")

	var age = 41
	var minor = age < 18

	fmt.Printf("At age %v, am i a minor? %v\n", age, minor)

}
