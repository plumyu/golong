package main

import (
	"fmt"
	"math/big"
)

// go 大数可以引入 big 包
// 一旦使用了 big.Int, 那么等式里其它的部分也必须使用 big.Int
// NewInt() 函数可以把 int64 转化为 big.Int 类型
func main() {
	lightSpeed := big.NewInt(299792)
	secondsPerDay := big.NewInt(86400)
	// 使用方法， 速度慢
	distance := new(big.Int)
	distance.SetString("240000000000000000000000000000", 10)
	fmt.Println("Andromeda Galaxy is", distance, "km away.")

	seconds := new(big.Int)
	seconds.Div(distance, lightSpeed)

	days := new(big.Int)
	days.Div(seconds, secondsPerDay)
	fmt.Println("That is", days, "days of travel at light speed.")
}
